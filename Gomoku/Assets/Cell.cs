﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class CellHandlers{
	public static Cell[,] Copy(this Cell[,] array){
		Cell[,] tempArray = new Cell[21,21];

		for (int i = 0; i < 21; i++) {
			for (int j = 0; j < 21; j++) {
				tempArray [i, j] = new Cell ((int)array [i, j].GetAfflication ());
			}
		}

		return tempArray;
	}
}

public class Cell{
	GameObject CellPrefab;
	public Player.Afflication afflication;

	public Cell(double x, double z, GameObject prefab){
		CellPrefab = GameObject.Instantiate (prefab, new Vector3((float)x, 0, (float)z), new Quaternion()) as GameObject;
		CellPrefab.GetComponent<PlanePrefabScript> ().cellReference = this;
		afflication = Player.Afflication.Neutral;
		CellPrefab.GetComponent<PlanePrefabScript> ().afflication = afflication;
	}

	public Cell(){}

	public Cell(int _afflication){
		CellPrefab = null;
		afflication = (Player.Afflication)_afflication;
	}

	public void SetAfflication(Player.Afflication _afflication){
		afflication = _afflication;
		if(CellPrefab != null)
			CellPrefab.GetComponent<PlanePrefabScript> ().SetColor (afflication);
	}

	public Player.Afflication GetAfflication(){
		return afflication;
	}
}
