﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FieldHandlers{
	public static class Handlers{
		public static bool HaveClaimedCell(int x, int y, Cell[,] field){
			for (int i = x - 1; i <= x + 1; i++) {
				for (int j = y - 1; j <= y + 1; j++) {
					if (OutOfRange(i, j) || SelfCell(i, j, x, y) || field [i, j].GetAfflication () == Player.Afflication.Neutral) {
						continue;
					}
					return true;
				}
			}
			return false;
		}

		public static bool CheckAlliedCells(int n, int m, Cell[,] field){
			for (int i = n - 1; i <= n + 1; i++) {
				for (int j = m - 1; j <= m + 1; j++) {
					if (OutOfRange(i, j) || SelfCell(i, j, n, m) || field [i, j].GetAfflication () != Player.Afflication.AI) {
						continue;
					}
					return true;
				}
			}
			return false;
		}

		public static bool OutOfRange(int x, int y){
			return x < 0 || y < 0 || x > 20 || y > 20;
		}

		public static bool SelfCell(int x, int y, int i, int j){
			return x == i && y == j;
		}

		public static bool CheckEnemyCells(int n, int m, Cell[,] field){
			for (int i = -1; i <= 0; i++) {
				for (int j = -1; j <= 1; j++) {
					if (SelfCell(i, j, 0, 0)) {
						return false;
					}

					int itemsInSequence = 0;

					for (int delta = 1; delta <= 4; delta++) {
						if (!OutOfRange (n + i * delta, m + j * delta) && field [n + i * delta, m + j * delta].GetAfflication () != Player.Afflication.Human)
							break;
						itemsInSequence++;
					}

					for (int delta = 1; delta <= 4; delta++) {
						if (!OutOfRange (n + i * (-delta), m + j * (-delta)) && field [n + i * (-delta), m + j * (-delta)].GetAfflication () != Player.Afflication.Human)
							break;
						itemsInSequence++;
					}

					if (itemsInSequence > 1)
						return true;
				}
			}
			return false;
		}
	}
}


public class TurnParameter{
	public int x;
	public int y;
	public int heuristic = -99999;
}

public class AI : MonoBehaviour {

	public Core coreReference; 

	public void FirstMove(){
		coreReference.field [21 / 2, 21 / 2].SetAfflication (Player.Afflication.AI);
		Checker.CheckForWinningCombination (Player.Afflication.AI, coreReference.field);
	}

	public void AITurn(int depth){
		TurnParameter bestTurn = new TurnParameter();
		for (int i = 0; i < 21; i++) {
			for (int j = 0; j < 21; j++) {
				if (coreReference.field [i, j].GetAfflication () == Player.Afflication.Neutral && FieldHandlers.Handlers.HaveClaimedCell (i, j, coreReference.field)) {
					Node node = Node.CreateNode(coreReference.field, i, j, depth - 1, Player.Afflication.AI, true);
					int heuristic = node.AlphaBeta (Player.Afflication.Human);
					if (heuristic >= bestTurn.heuristic) {
						bestTurn.x = node.x;
						bestTurn.y = node.y;
						bestTurn.heuristic = heuristic;
					}
					System.GC.Collect (1, System.GCCollectionMode.Forced);
				}
			}
		}

		coreReference.field [bestTurn.x, bestTurn.y].SetAfflication (Player.Afflication.AI);
		Checker.CheckForWinningCombination (Player.Afflication.AI, coreReference.field);
	}
}
