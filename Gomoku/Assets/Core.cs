﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player{
	public enum Afflication{
		Neutral = 0,
		Human = 1,
		AI = -1
	}
}

public class Core : MonoBehaviour {
	public Cell[,] field;
	public GameObject prefab;
	public bool isPlayerTurn = false;
	PlayerScript playerScript;
	AI ai;
	[SerializeField]int depth = 2;

	void Start(){
		field = new Cell[21, 21];
		for (int i = 0; i < 21; i++) {
			for (int j = 0; j < 21; j++) {
				field [i, j] = new Cell ((-13.125 + 1.25 * j), (13.125 - 1.25 * i), prefab);
			}
		}

		playerScript = GameObject.Find ("Main Camera").GetComponent<PlayerScript> ();
		ai = GetComponent<AI> ();
		ai.coreReference = this;

		Player.Afflication player = new Player.Afflication();
		switch(1){
			case 1:
				player = (Player.Afflication)1;
				NextTurn (player);
				break;
			case 2:
				Debug.Log ("Its AI!");
				ai.FirstMove ();
				break;
		}
	}

	public void NextTurn(Player.Afflication afflication){
		switch (afflication) {
			case Player.Afflication.AI:
				Debug.Log ("AI Turn!");
				ai.AITurn (depth);
				break;
			case Player.Afflication.Human:
				Debug.Log ("Player Turn!");
				playerScript.StartPlayerTurn ();
				break;
		}
	}
}