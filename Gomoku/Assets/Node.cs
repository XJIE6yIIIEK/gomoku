﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

	List<Node> childs = new List<Node> ();
	Cell[,] localField = new Cell[21,21];
	public int x;
	public int y;
	public int heuristic;
	public int enemyH;
	public int friendH;

	public int AlphaBeta(Player.Afflication afflication){
		if (childs.Count == 0) {
			return heuristic;
		}

		int score = 0;
		foreach (Node node in childs) {
			int s = node.AlphaBeta ((Player.Afflication)(-(int)afflication));
			if (afflication == Player.Afflication.AI) {
				if (s > score)
					score = s;
			}
			if (afflication == Player.Afflication.Human) {
				if (s < score)
					score = s;
			}
		}
		return score;
	}

	int GetSequenceHeuristic(bool isBorderedSequence, bool isClosedSequence, bool haveEmptySpace, int itemsInSequence){
		int heuristicOfSequence = 0;
		switch (itemsInSequence) {
			//500-1000
			case 4:
				if (isBorderedSequence && !isClosedSequence)
					heuristicOfSequence += 500;
				else if (haveEmptySpace)
					heuristicOfSequence += 750;
				else if (!isBorderedSequence && !haveEmptySpace)
					heuristicOfSequence += 1000;
				break;

			//100-200
			case 3:
				if (isBorderedSequence && !isClosedSequence)
					heuristicOfSequence += 100;
				else if (haveEmptySpace)
					heuristicOfSequence += 150;
				else if (!isBorderedSequence && !haveEmptySpace)
					heuristicOfSequence += 200;
				break;

			//5-50
			case 2:
				if (isBorderedSequence && !isClosedSequence)
					heuristicOfSequence += 5;
				else if (haveEmptySpace)
					heuristicOfSequence += 30;
				else if (!isBorderedSequence && !haveEmptySpace)
					heuristicOfSequence += 50;
				break;
		}

		return heuristicOfSequence;
	}

	//enemy
	int GetSequenceHeuristic1(bool isBorderedSequence, bool isClosedSequence, bool haveEmptySpace, int itemsInSequence){
		int heuristicOfSequence = 0;

		switch (itemsInSequence) {
			case 4:
				if (isBorderedSequence && !isClosedSequence)
					heuristicOfSequence += 1100;
				else if (haveEmptySpace)
					heuristicOfSequence += 1600;
				else if (!isBorderedSequence && !haveEmptySpace)
					heuristicOfSequence += 2100;
				break;

			case 3:
				if (isBorderedSequence && !isClosedSequence)
					heuristicOfSequence += 150;
				else if (haveEmptySpace)
					heuristicOfSequence += 200;
				else if (!isBorderedSequence && !haveEmptySpace)
					heuristicOfSequence += 450;
				break;

			case 2:
				if (isBorderedSequence && !isClosedSequence)
					heuristicOfSequence += 20;
				else if (haveEmptySpace)
					heuristicOfSequence += 70;
				else if (!isBorderedSequence && !haveEmptySpace)
					heuristicOfSequence += 90;
				break;
		}

		return heuristicOfSequence;
	}

	int GetEnemySequenceHeuristic(Player.Afflication _afflication){
		int maxSequenceHeuristic = 0;

		for (int i = 0; i < 21; i++) {
			for (int j = 0; j < 21; j++) {

				if (localField [i, j].GetAfflication () == (Player.Afflication)(-(int)_afflication)) {

					for (int dirX = 0; dirX <= 1; dirX++) {
						for (int dirY = -1; dirY <= 1; dirY++) {
							if (FieldHandlers.Handlers.SelfCell (dirX, dirY, 0, 0))
								continue;

							bool isBorderedSequence = false;
							bool isClosedSequence = false;
							bool haveEmptySpace = false;

							int itemsInSequence = 1;
							for (int depth = 1; depth <= 4; depth++) {
								if (FieldHandlers.Handlers.OutOfRange (i + dirX * depth, j + dirY * depth)) {
									isBorderedSequence = true;
									break;
								}
								Player.Afflication afflication = localField [i + dirX * depth, j + dirY * depth].GetAfflication ();
								if (afflication == _afflication) {
									isBorderedSequence = true;
									break;
								} else if (afflication == Player.Afflication.Neutral && !haveEmptySpace) {
									if (FieldHandlers.Handlers.OutOfRange (i + dirX * (depth + 1), j + dirY * (depth + 1)) ||
										localField [i + dirX * (depth + 1), j + dirY * (depth + 1)].GetAfflication () != (Player.Afflication)(-(int)_afflication))
										break;

									haveEmptySpace = true;
									continue;
								} else if (afflication == Player.Afflication.Neutral && haveEmptySpace) {
									break;
								}
								itemsInSequence++;
							}

							if (FieldHandlers.Handlers.OutOfRange (i - dirX, j - dirY) || localField [i - dirX, j - dirY].GetAfflication () != Player.Afflication.Neutral) {
								if (isBorderedSequence)
									isClosedSequence = true;
								else
									isBorderedSequence = true;
							}

							int sequenceHeuristic = GetSequenceHeuristic1 (isBorderedSequence, isClosedSequence, haveEmptySpace, itemsInSequence);
							if (sequenceHeuristic > maxSequenceHeuristic)
								maxSequenceHeuristic = sequenceHeuristic;
						}
					}
				}
			}
		}

		return maxSequenceHeuristic;
	}

	int GetAlliedSequenceHeuristic(Player.Afflication _afflication){
		int maxAlliedHeuristic = 0;

		for (int i = -1; i <= 0; i++) {
			for (int j = -1; j <= 1; j++) {
				if (FieldHandlers.Handlers.SelfCell (i, j, 0, 0) || (i == 0 && j == 1))
					continue;

				int itemsInSequence = 1;
				bool isBorderedSequence = false;
				bool isClosedSequence = false;
				bool haveEmptySpace = false;

				for (int delta = 1; delta <= 4; delta++) {
					if (FieldHandlers.Handlers.OutOfRange (x + i * delta, y + j * delta)) {
						isBorderedSequence = true;
						break;
					}

					Player.Afflication afflication = localField [x + i * delta, y + j * delta].GetAfflication ();
					if (afflication == (Player.Afflication)(-(int)_afflication)) {
						isBorderedSequence = true;
						break;
					} else if (afflication == Player.Afflication.Neutral && !haveEmptySpace) {
						if (FieldHandlers.Handlers.OutOfRange (x + i * (delta+1), y + j * (delta+1)) ||
							localField [x + i * (delta+1), y + j * (delta+1)].GetAfflication () != _afflication)
							break;

						haveEmptySpace = true;
						continue;
					} else if (afflication == Player.Afflication.Neutral && haveEmptySpace){
						break;
					}

					itemsInSequence++;
				}

				for (int delta = 1; delta <= 4; delta++) {
					if (FieldHandlers.Handlers.OutOfRange (x - i * delta, y - j * delta)) {
						if (isBorderedSequence)
							isClosedSequence = true;
						else
							isBorderedSequence = true;
						break;
					}

					Player.Afflication afflication = localField [x - i * delta, y - j * delta].GetAfflication ();
					if (afflication == (Player.Afflication)(-(int)_afflication)) {
						if (isBorderedSequence)
							isClosedSequence = true;
						else
							isBorderedSequence = true;
						break;
					} else if (afflication == Player.Afflication.Neutral && !haveEmptySpace){
						if (FieldHandlers.Handlers.OutOfRange (x - i * (delta + 1), y - j * (delta + 1)) ||
							localField [x - i * (delta + 1), y - j * (delta + 1)].GetAfflication () != _afflication)
							break;

						haveEmptySpace = true;
						continue;
					} else if(afflication == Player.Afflication.Neutral && haveEmptySpace){
						break;
					}
					itemsInSequence++;
				}

				int sequenceHeuristic = GetSequenceHeuristic (isBorderedSequence, isClosedSequence, haveEmptySpace, itemsInSequence);
				if (sequenceHeuristic > maxAlliedHeuristic)
					maxAlliedHeuristic = sequenceHeuristic;
			}
		}

		return maxAlliedHeuristic;
	}

	public void GetHeuristic(Player.Afflication afflication){
		friendH = GetAlliedSequenceHeuristic (afflication);
		enemyH = GetEnemySequenceHeuristic (afflication);
		heuristic = friendH - enemyH;
	}

	private Node(Cell[,] field, int m, int n, int depth, Player.Afflication _afflication){
		x = m; y = n;
		localField = field;

		if (Checker.CheckForWinningCombination (Player.Afflication.AI, localField, true)) {
			heuristic = 10000;
			return;
		}

		if (Checker.CheckForWinningCombination (Player.Afflication.Human, localField, true)) {
			heuristic = -20000;
			return;
		}

		if (depth < 1) {
			GetHeuristic (_afflication);
			if (_afflication == Player.Afflication.Human)
				heuristic *= -1;
			return;
		}

		for (int i = 0; i < 21; i++) {
			for (int j = 0; j < 21; j++) {
				if (localField [i, j].GetAfflication () == Player.Afflication.Neutral && FieldHandlers.Handlers.HaveClaimedCell (i, j, localField)) {
					Node node = Node.CreateNode(localField, i, j, depth - 1, (Player.Afflication)(-(int)_afflication), false);
					if(node != null)
						childs.Add (node);
				}
			}
		}

		if (childs.Count == 0)
			GetHeuristic (_afflication);
	}

	public static Node CreateNode(Cell[,] field, int m, int n, int depth, Player.Afflication _afflication, bool descisionTreeRoot){
		Cell[,] _localField = field.Copy ();
		_localField [m, n].SetAfflication (_afflication);

		if (_afflication == Player.Afflication.AI && !descisionTreeRoot) {
			if (!FieldHandlers.Handlers.CheckAlliedCells (m, n, _localField)) {
				if (!FieldHandlers.Handlers.CheckEnemyCells (m, n, _localField)) {
					Debug.Log ("Dont created");
					return null;
				}
			}
		}

		return new Node (_localField, m, n, depth, _afflication);
	}
}
