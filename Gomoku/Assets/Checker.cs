﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class Checker {
	static Core core = GameObject.Find("GameCore").GetComponent<Core>();

	public static bool FindWinningCombinationOnDirection(int dirX, int dirY, int x, int y, Player.Afflication afflication, Cell[,] field){
		if (x + 3 * dirX >= 21 || y + 3 * dirY >= 21 || x + 3 * dirX < 0 || y + 3 * dirY < 0)
			return false;

		for (int i = 1; i <= 3; i++) {
			if (field [x + i * dirX, y + i * dirY].GetAfflication () != afflication)
				return false;
		}
		return true;
	}

	public static bool IsThereWinningCombination(int x, int y, Player.Afflication afflication, Cell[,] field){
		for (int i = x; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (i >= 21 || j >= 21 || j < 0 || i < 0|| field [i, j].GetAfflication () != afflication || (i == x && j == y))
					continue;
				
				if (FindWinningCombinationOnDirection (i - x, j - y, i, j, afflication, field))
					return true;
			}
		}
		return false;
	}

	public static bool CheckForWinningCombination(Player.Afflication afflication, Cell[,] field, bool checkTerminal = false){
		for (int i = 0; i < 21; i++) {
			for (int j = 0; j < 21; j++) {
				if (field [i, j].GetAfflication () != afflication)
					continue;
				
				if (IsThereWinningCombination (i, j, afflication, field)) {
					if (checkTerminal)
						return true;
					else {
						Debug.Log ("Player " + (int)afflication + " win!");
						return false;
					}
				}
			}
		}

		if (checkTerminal == false) {
			core.NextTurn ((Player.Afflication)(-(int)afflication));
		}
		return false;
	}
}
