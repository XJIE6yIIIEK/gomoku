﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanePrefabScript : MonoBehaviour {

	public Player.Afflication afflication;
	public Cell cellReference;
	public Material playerMaterial;
	public Material aiMaterial;
	public Material neutralMaterial;
	public Material scopedMaterial;

	void Awake(){
		GetComponent<MeshRenderer> ().material = neutralMaterial;
	}

	void Update(){
		afflication = cellReference.afflication;
	}

	void OnMouseEnter(){
		if(cellReference.GetAfflication() == Player.Afflication.Neutral)
			GetComponent<MeshRenderer> ().material = scopedMaterial;
	}

	void OnMouseExit(){
		if(cellReference.GetAfflication() == Player.Afflication.Neutral)
			GetComponent<MeshRenderer> ().material = neutralMaterial;
	}

	public void SetColor(Player.Afflication afflication){
		if (afflication == Player.Afflication.AI) {
			GetComponent<MeshRenderer> ().material = aiMaterial;
		} else {
			GetComponent<MeshRenderer> ().material = playerMaterial;
		}
	}
}
