﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	Core coreReference;
	private IEnumerator playerTurn;

	void Start(){
		coreReference = GameObject.Find ("GameCore").GetComponent<Core> ();
	}

	public void StartPlayerTurn(){
		playerTurn = PlayerTurn();
		coreReference.isPlayerTurn = true;
		StartCoroutine (playerTurn);
	}

	IEnumerator PlayerTurn(){
		while (true) {
			if (Input.GetMouseButtonDown (0) && coreReference.isPlayerTurn) {
				coreReference.isPlayerTurn = false;
				RaycastHit _hit;
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray, out _hit, 1000) && _hit.collider.GetComponent<PlanePrefabScript> ().cellReference.GetAfflication () == Player.Afflication.Neutral) {
					_hit.collider.GetComponent<PlanePrefabScript>().cellReference.SetAfflication(Player.Afflication.Human);
					Checker.CheckForWinningCombination (Player.Afflication.Human, coreReference.field);
					yield break;
				}
				coreReference.isPlayerTurn = true;
			}
			yield return null;
		}
	}
}
